import React, { useState } from "react";
import { Desktop } from "../../common/responsive";
import "./header.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { FaArrowDown } from "react-icons/fa";
import { IoMdPlayCircle } from "react-icons/io";
import { AiOutlineMenuUnfold } from "react-icons/ai";
import { IoHeartCircleSharp } from "react-icons/io5";
import { RiArrowDropDownFill } from "react-icons/ri";
import { GiElectric } from "react-icons/gi";
import { Row, Col } from "react-bootstrap";

// class Header extends Component {
const Header = () =>{
    const [toggle, setToggle] = useState(true);

    return (
      <>
        <Desktop>
          <div className="sp-full-container-header">
            <div className="sp-container-header">
              <Row className="sp-row-header">
                <Col sm={6} id="sp-col-logo">
                  <img
                    src="./assset/Spotify Logo.png"
                    alt=""
                    style={{
                      width: "132px",
                      height: "41px",
                    }}
                  />
                </Col>
                <Col sm={1} id="sp-text-premium">
                  <a href="..">Premium</a>
                </Col>
                <Col sm={1} id="sp-text-premium">
                  <a href="..">Support</a>
                </Col>
                <Col sm={1} id="sp-text-premium">
                  <a href="..">Download</a>
                </Col>
                <Col sm={1} id="sp-text-premium">
                  <a href="..">Sign up</a>
                </Col>
                <Col sm={1} id="sp-text-premium">
                  <a href="..">Log in</a>
                </Col>
              </Row>
            </div>
            <div id="sp-phone-container">
              <Row className="sp-row-phone">
                <div id="sp-round-container-1">
                  <div id="sp-phone-body">
                    <span className="sp-screenshot">
                      <img
                        src="./assset/screen_shot_taste_profile.jpg"
                        alt=""
                        style={{
                          width: "262px",
                          height: "450px",
                        }}
                      />
                    </span>
                  </div>
                </div>
                <div id="sp-round-container-2"></div>
              </Row>
            </div>

            <div id="sp-why-container">
              <div id="sp-round-circle">
                <FaArrowDown size="25px" />
              </div>
              <div className="sp-why-text">Why Spotify?</div>
              <Row className="sp-row-playlist">
                <Col sm={2} id="sp-col-playlist">
                  <div className="sp-round1-playlist">
                    <IoMdPlayCircle size="75px" />
                  </div>
                  <span id="sp-play-font">Play your favorites.</span>
                  <div id="sp-play-text">
                    Listen to the songs you love, and discover new music and
                    podcasts.
                  </div>
                </Col>
                <Col sm={2} id="sp-col-playlist">
                  <div className="sp-round1-playlist">
                    <AiOutlineMenuUnfold size="75px" />
                  </div>
                  <span id="sp-play-font">Playlists made easy.</span>
                  <div id="sp-play-text">
                    We'll help you make playlists. Or enjoy playlists made by
                    music experts.
                  </div>
                </Col>
                <Col sm={2} id="sp-col-playlist">
                  <div className="sp-round1-playlist">
                    <IoHeartCircleSharp size="75px" />
                  </div>
                  <span id="sp-play-font">Make it yours.</span>
                  <div id="sp-play-text">
                    Tell us what you like, and we'll recommend music for you.
                  </div>
                </Col>
                <Col sm={2} id="sp-col-playlist">
                  <div className="sp-round1-playlist">
                    <GiElectric size="75px" />
                  </div>
                  <span id="sp-play-font">Save mobile data.</span>
                  <div id="sp-play-text">
                    To use less data when you play music, turn on Data Saver in
                    Settings.
                  </div>
                </Col>
              </Row>
            </div>

            <div className="sp-credit-container">
              <Row className="sp-credit-row">
                <Col className="sp-credit-col">
                  <div className="sp-credit-text ">It's free.</div>
                  <span className="sp-credit-text">
                    No credit card required.
                  </span>
                </Col>
              </Row>
            </div>

            <div className="sp-questions-container">
              <div className="sp-questions-text mt-5">Got questions?</div>
              <button type="button" class="collapsible" value={toggle}
              onClick={(e) => {
                setToggle(e.target.value);
              }}>
                How do I create a playlist? <RiArrowDropDownFill id ="sp-toggle-icon" size="35px"/>
              </button>
              
              <div class="content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
              <button type="button" class="collapsible" value={toggle}
              onClick={(e) => {
                setToggle(e.target.value);
              }}>
                How do I create a playlist? <RiArrowDropDownFill id ="sp-toggle-icon" size="35px"/>
              </button>
              
              <div class="content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>

              <button type="button" class="collapsible" value={toggle}
              onClick={(e) => {
                setToggle(e.target.value);
              }}>
                How do I create a playlist? <RiArrowDropDownFill id ="sp-toggle-icon" size="35px"/>
              </button>
              
              <div class="content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>

              <button type="button" class="collapsible" value={toggle}
              onClick={(e) => {
                setToggle(e.target.value);
              }}>
                How do I create a playlist? <RiArrowDropDownFill id ="sp-toggle-icon" size="35px"/>
              </button>
              
              <div class="content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>

              <div className="sp-ready-container mt-5">
              <Row className="sp-credit-row">
                <Col className="sp-credit-col">
                  <div className="sp-credit-text ">Ready? Let's play.</div>
                  <button type="button" id="sp-get-spotify"><span id="sp-get-font">GET SPOTIFY FREE</span></button>
                </Col>
              </Row>
              </div>
            </div>
          </div>
        </Desktop>
      </>
    );
  }


export default Header;
